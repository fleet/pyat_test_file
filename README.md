# Pyat Test File

This project contains test files used by the [pyat](https://gitlab.ifremer.fr/fleet/pyat) project and associates




## Deployment

Tests files are availables with the generic package registry of gitlab. 
To retrieve the file use the following commands 

curl "https://gitlab.ifremer.fr/api/v4/projects/1830/packages/generic/pyat_test_file/0.0.1/pyat_test_file.zip" --output pyat_test_file.zip 

To push a new file release use the following commands 

```
curl --user "NAME:PASSWD" --upload-file pyat_test_file.zip "https://gitlab.ifremer.fr/api/v4/projects/1830/packages/generic/pyat_test_file/0.0.1/pyat_test_file.zip?select=package_file"
```


